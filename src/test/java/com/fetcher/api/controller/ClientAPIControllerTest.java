package com.fetcher.api.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fetcher.api.dto.PositionDTO;
import com.fetcher.api.exception.PositionServiceException;
import com.fetcher.api.service.IPositionService;

import junit.framework.TestCase;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

@RunWith(JMockit.class)
public class ClientAPIControllerTest extends TestCase {
	@Injectable
	private IPositionService positionService; 

	@Tested(availableDuringSetup = true)
	private ClientAPIController clientAPIController;

	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(clientAPIController).build();
	}

	@Test
	public void getEmptyPositionsOK() throws Exception {
		List<PositionDTO> positionsDTOList = new ArrayList<>();
		new Expectations() {{
			positionService.getPositions(null, null, null, "Apple");
			result = positionsDTOList;
		}};		

		MvcResult result = this.mockMvc.perform(get("/api/positions/Apple"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		String content = result.getResponse().getContentAsString();
		assertEquals(content, "[]");
	}

	/**
	 * Filtered by location
	 * @throws Exception
	 */
	@Test
	public void getSomePositions1OK() throws Exception {
		List<PositionDTO> positionsDTOList = createPositionDTOList();
		
		String location = "New York";
		new Expectations() {{
			positionService.getPositions(location, null, null, "Apple");
			result = positionsDTOList;
		}};		

		this.mockMvc.perform(get("/api/positions/Apple").param("location", location))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("[{'externalId':'95444272-e527-4c9b-81ec-3adb0173c089','name':'Full Stack Developer','currentCompany':'Apple Inc.','type':'Full Time','location':'St.Leon-Rot, Germany'}]"))
				.andReturn();
	}
	
	/**
	 * Filtered by location and Name
	 * @throws Exception
	 */
	@Test
	public void getSomePositions2OK() throws Exception {
		List<PositionDTO> positionsDTOList = createPositionDTOList();
		
		String location = "New York";
		String name = "Full Stack";
		new Expectations() {{
			positionService.getPositions(location, name, null, "Apple");
			result = positionsDTOList;
		}};		

		this.mockMvc.perform(get("/api/positions/Apple").param("location", location).param("name", name))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("[{'externalId':'95444272-e527-4c9b-81ec-3adb0173c089','name':'Full Stack Developer','currentCompany':'Apple Inc.','type':'Full Time','location':'St.Leon-Rot, Germany'}]"))
				.andReturn();
	}
	
	/**
	 * Filtered by location, Name and Type
	 * @throws Exception
	 */
	@Test
	public void getSomePositions3OK() throws Exception {
		List<PositionDTO> positionsDTOList = createPositionDTOList();
		
		String location = "New York";
		String name = "Full Stack";
		String type = "Full Time";
		new Expectations() {{
			positionService.getPositions(location, name, type, "Apple");
			result = positionsDTOList;
		}};		

		this.mockMvc.perform(get("/api/positions/Apple").param("location", location).param("name", name).param("type", type))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().json("[{'externalId':'95444272-e527-4c9b-81ec-3adb0173c089','name':'Full Stack Developer','currentCompany':'Apple Inc.','type':'Full Time','location':'St.Leon-Rot, Germany'}]"))
				.andReturn();
	}
	
	/**
	 * Test fail . Get positions without Client name
	 * @throws Exception
	 */
	@Test
	public void getPositionsFail() throws Exception {
		
		this.mockMvc.perform(get("/api/positions"))
				.andDo(print())
				.andExpect(status().isNotFound())
				.andReturn();
	}
	
	/**
	 * Internal Server error
	 * @throws Exception
	 */
	@Test
	public void getInternalServerError() throws Exception {
		
		String location = "New York";
		new Expectations() {{
			positionService.getPositions(location, null, null, "Apple");
			result = new PositionServiceException();
		}};		

		this.mockMvc.perform(get("/api/positions/Apple").param("location", location))
				.andDo(print())
				.andExpect(status().isInternalServerError())
				.andReturn();
	}	
	
	private List<PositionDTO> createPositionDTOList() {
		List<PositionDTO> positions = new ArrayList<>();

		positions.add(new PositionDTO("95444272-e527-4c9b-81ec-3adb0173c089", "Full Stack Developer","Apple Inc.", "Full Time", "St.Leon-Rot, Germany"));
		return positions;
	}
}
