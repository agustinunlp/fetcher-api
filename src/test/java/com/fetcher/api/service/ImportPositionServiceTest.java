package com.fetcher.api.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.fetcher.api.dao.IPositionDAO;
import com.fetcher.api.dto.ImportPositionDTO;
import com.fetcher.api.exception.PositionRepositoryException;
import com.fetcher.api.repository.IPositionRepository;
import com.fetcher.api.service.impl.ImportPositionServiceImpl;

import junit.framework.TestCase;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

/**
 * Test for Importing Position Service 
 * @author agustin
 *
 */
@RunWith(JMockit.class)
public class ImportPositionServiceTest extends TestCase {

	@Tested
	private ImportPositionServiceImpl importPositionService;

	@Injectable
	private IPositionDAO positionDAO;

	@Injectable 
	private IPositionRepository positionsRepository;


	@Test
	public void importPositions(){
		String url="https://jobs.github.com/positions.json";
		List<ImportPositionDTO> positionsDTOList = buildImportPositionDTOList();		 

		try {
			new Expectations() {{
				positionsRepository.getPositions(anyString, anyInt);
				result = positionsDTOList;
			}};
		} catch (PositionRepositoryException e1) {
			e1.printStackTrace();
		}

		try {
			importPositionService.importPositions(url, 2);
		} catch (PositionRepositoryException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = PositionRepositoryException.class)
	public void importPositionsRepositoryFail() throws PositionRepositoryException{
		String url="https://jobs.github.com/positions.json";

		new Expectations() {{
			positionsRepository.getPositions(url, anyInt);
			result = new PositionRepositoryException();
		}};

		importPositionService.importPositions(url, 2);
	}

	private List<ImportPositionDTO> buildImportPositionDTOList() {
		List<ImportPositionDTO> list = new ArrayList<>();
		list.add(new ImportPositionDTO("95444272-e527-4c9b-81ec-3adb0173c089","Full Time","Apple Inc.","New York", "Java Developer Sr"));
		list.add(new ImportPositionDTO("95444272-e527-4c9b-81ec-3adb0173c090","Full Time","Apple Inc.","New York", "Angular Developer Sr"));
		list.add(new ImportPositionDTO("95444272-e527-4c9b-81ec-3adb0173c091","Full Time","Apple Inc.","New York", "Ruby Developer Sr"));
		list.add(new ImportPositionDTO("95444272-e527-4c9b-81ec-3adb0173c092","Contract","Apple Inc.","Madrid", "Python Developer Sr"));
		list.add(new ImportPositionDTO("95444272-e527-4c9b-81ec-3adb0173c093","Contract","Apple Inc.","Madrid", "React Native Developer Sr"));
		return list;
	}

}
