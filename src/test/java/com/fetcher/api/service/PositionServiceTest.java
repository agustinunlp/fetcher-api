package com.fetcher.api.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.fetcher.api.dao.IClientDAO;
import com.fetcher.api.domain.Client;
import com.fetcher.api.domain.Position;
import com.fetcher.api.domain.PositionType;
import com.fetcher.api.dto.PositionDTO;
import com.fetcher.api.exception.DAOException;
import com.fetcher.api.exception.PositionServiceException;
import com.fetcher.api.service.impl.PositionServiceImpl;

import junit.framework.TestCase;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;

/**
 * Test for Position Service 
 * @author agustin
 *
 */

@RunWith(JMockit.class)
public class PositionServiceTest extends TestCase {
	
	private static final String JAVA = "Java";

	private static final String FULL_TIME = "Full Time";

	private static final String NEW_YORK = "New York";

	private static final String APPLE_INC = "Apple Inc.";

	@Tested
	private PositionServiceImpl positionService;
	
	@Injectable
	private IClientDAO clientDAO;

	
	/**
	 * Retrieve all positions from Apple Inc.
	 * without filters
	 * @throws DAOException
	 * @throws PositionServiceException
	 */
	@Test
	public void getAllPositions() throws DAOException, PositionServiceException{
		Client client = buildClient(1l, APPLE_INC);
		String location="";
		String name ="";
		String type ="";
		String clientName=APPLE_INC;	
		 
		new Expectations() {{
			clientDAO.getClientByName(APPLE_INC);
			result = client;
		}};
		   
		
		List<PositionDTO> result = positionService.getPositions(location, name, type, clientName);
		
		assertEquals(result.size(), 6);
		assertEquals(result.get(0).getExternalId(), "95444272-e527-4c9b-81ec-3adb0173c089");
		assertEquals(result.get(0).getCurrentCompany(), APPLE_INC);
		assertEquals(result.get(0).getLocation(), "St.Leon-Rot, Germany");
		assertEquals(result.get(0).getName(), "Full Stack Developer");
		assertEquals(result.get(0).getType(), FULL_TIME);		
	}

	/**
	 * Retrieve all positions from Apple Inc.
	 * Filtered by location
	 * @throws DAOException
	 * @throws PositionServiceException
	 */
	@Test
	public void getPositionsFilteredByLocation() throws DAOException, PositionServiceException{
		Client client = buildClient(1l, APPLE_INC);
		String location=NEW_YORK;
		String name ="";
		String type ="";
		String clientName=APPLE_INC;	
		 
		new Expectations() {{
			clientDAO.getClientByName(APPLE_INC);
			result = client;
		}};
		   
		
		List<PositionDTO> result = positionService.getPositions(location, name, type, clientName);
		
		assertEquals(result.size(), 3);
		assertEquals(result.get(0).getLocation(), NEW_YORK);
		assertEquals(result.get(1).getLocation(), NEW_YORK);
		assertEquals(result.get(2).getLocation(), NEW_YORK);
	}

	/**
	 * Retrieve all positions from Apple Inc.
	 * Filtered by position type
	 * @throws DAOException
	 * @throws PositionServiceException
	 */
	@Test
	public void getPositionsFilteredByType() throws DAOException, PositionServiceException{
		Client client = buildClient(1l, APPLE_INC);
		String location="";
		String name ="";
		String type =FULL_TIME;
		String clientName=APPLE_INC;	
		 
		new Expectations() {{
			clientDAO.getClientByName(APPLE_INC);
			result = client;
		}};
		   
		
		List<PositionDTO> result = positionService.getPositions(location, name, type, clientName);
		
		assertEquals(result.size(), 4);
		assertEquals(result.get(0).getType(), FULL_TIME);
		assertEquals(result.get(1).getType(), FULL_TIME);
		assertEquals(result.get(2).getType(), FULL_TIME);
		assertEquals(result.get(3).getType(), FULL_TIME);
	}
	
	/**
	 * Retrieve all positions from Apple Inc.
	 * Filtered by position name
	 * @throws DAOException
	 * @throws PositionServiceException
	 */
	@Test
	public void getPositionsFilteredByName() throws DAOException, PositionServiceException{
		Client client = buildClient(1l, APPLE_INC);
		String location="";
		String name =JAVA;
		String type ="";
		String clientName=APPLE_INC;	
		 
		new Expectations() {{
			clientDAO.getClientByName(APPLE_INC);
			result = client;
		}};
		   
		
		List<PositionDTO> result = positionService.getPositions(location, name, type, clientName);
		
		assertEquals(result.size(), 2);
		assertTrue(result.get(0).getName().contains(JAVA));
		assertTrue(result.get(1).getName().contains(JAVA));
	}
	
	/**
	 * Retrieve all positions from Apple Inc.
	 * Filtered by position name
	 * @throws DAOException
	 * @throws PositionServiceException
	 */
	@Test
	public void getPositionsFilteredByNameAndLocation() throws DAOException, PositionServiceException{
		Client client = buildClient(1l, APPLE_INC);
		String location=NEW_YORK;
		String name =JAVA;
		String type ="";
		String clientName=APPLE_INC;	
		 
		new Expectations() {{
			clientDAO.getClientByName(APPLE_INC);
			result = client;
		}};		   
		
		List<PositionDTO> result = positionService.getPositions(location, name, type, clientName);
		
		assertEquals(result.size(), 1);
		assertTrue(result.get(0).getName().contains(JAVA));
		assertEquals(result.get(0).getLocation(), NEW_YORK);
	}


	private Client buildClient(Long id, String clientName) {
		Client client = new Client(id, clientName);
		List<Position>positions = buildPositions();
		client.setPositions(positions);
		
		return client;
	}

	private List<Position> buildPositions() {
		List<Position> positions = new ArrayList<Position>();
		positions.add(new Position("95444272-e527-4c9b-81ec-3adb0173c089", "Full Stack Developer", new Client(1l,APPLE_INC), new PositionType(FULL_TIME), "St.Leon-Rot, Germany"));
		positions.add(new Position("95444272-e527-4c9b-81ec-3adb0173c090", "Full Stack Developer", new Client(1l,APPLE_INC), new PositionType(FULL_TIME), NEW_YORK));
		positions.add(new Position("95444272-e527-4c9b-81ec-3adb0173c091", "Java Developer", new Client(1l,APPLE_INC), new PositionType("Contract"), "St.Leon-Rot, Germany"));
		positions.add(new Position("95444272-e527-4c9b-81ec-3adb0173c092", "Java Developer", new Client(1l,APPLE_INC), new PositionType("Contract"), NEW_YORK));
		positions.add(new Position("95444272-e527-4c9b-81ec-3adb0173c093", "Angular Developer", new Client(1l,APPLE_INC), new PositionType(FULL_TIME), "St.Leon-Rot, Germany"));
		positions.add(new Position("95444272-e527-4c9b-81ec-3adb0173c094", "Angular Developer", new Client(1l,APPLE_INC), new PositionType(FULL_TIME), NEW_YORK));

		return positions;
 	}
}