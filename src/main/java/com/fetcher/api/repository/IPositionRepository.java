package com.fetcher.api.repository;

import java.util.List;

import com.fetcher.api.dto.ImportPositionDTO;
import com.fetcher.api.exception.PositionRepositoryException;


public interface IPositionRepository {
	
	List<ImportPositionDTO> getPositions(String url, int page) throws PositionRepositoryException ;
}
