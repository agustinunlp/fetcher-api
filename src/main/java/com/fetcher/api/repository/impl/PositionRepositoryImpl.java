package com.fetcher.api.repository.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;

import com.fetcher.api.dto.ImportPositionDTO;
import com.fetcher.api.exception.PositionRepositoryException;
import com.fetcher.api.repository.IPositionRepository;

/**
 * Repository that retrieve positions from an API
 * @author agustin
 *
 */
@Repository
public class PositionRepositoryImpl  implements IPositionRepository{
	public static final Logger logger = LoggerFactory.getLogger(PositionRepositoryImpl.class);

	@Override
	public List<ImportPositionDTO> getPositions(String url, int page) throws PositionRepositoryException {
		try {
		
			RestTemplate restTemplate = new RestTemplate();
			
			ResponseEntity<List<ImportPositionDTO>> locationsResponse =
					restTemplate.exchange(url +"?page="+page,
							HttpMethod.GET, null, new ParameterizedTypeReference<List<ImportPositionDTO>>() {
					});
			List<ImportPositionDTO> locationList = locationsResponse.getBody();
			return locationList;
	
		} catch (IllegalArgumentException | HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException e) {
			logger.error("Error connecting with {} . {}", url, e.getMessage());
			throw new PositionRepositoryException(e);
		}
	}

}
