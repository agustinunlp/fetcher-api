package com.fetcher.api.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fetcher.api.dto.PositionDTO;

@Entity
@Table(name="position")
public class Position {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name = "externalId")
	private String externalId;
	
	@Column(name = "name")
	public String name;	
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn
	private PositionType type;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "fk_client")
	@JsonBackReference
	private Client currentCompany;
	
	@Column(name = "location")
	private String location;	


	public Position() {
		super();
	}

	public Position( String externalId, String name, Client currentCompany, PositionType type,
			String location) {
		super();
		
		this.externalId = externalId;
		this.name = name;
		this.currentCompany = currentCompany;
		this.type = type;
		this.location = location;
	}

	public void setValueFrom(Position position){
		this.name = position.getName();
		this.currentCompany = position.getCurrentCompany();
		this.type = position.getType();
		this.location = position.getLocation();
	}
	
	public PositionDTO convertToDTO(){
		return new PositionDTO(externalId, name, currentCompany.getName(), type.getType(), location);		
	}
	
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Client getCurrentCompany() {
		return currentCompany;
	}
	public void setCurrentCompany(Client currentCompany) {
		this.currentCompany = currentCompany;
	}
	public PositionType getType() {
		return type;
	}
	public void setType(PositionType type) {
		this.type = type;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
