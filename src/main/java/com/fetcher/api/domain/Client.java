package com.fetcher.api.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="client")
public class Client {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "id")
	private Long clientId;
	
	@Column(name = "name")
	private String name;

	@OneToMany
	@JoinColumn(name = "fk_client")
	@JsonManagedReference
	private List<Position> positions;
	 
	public Client(Long clientId, String name) {
		super();
		this.clientId = clientId;
		this.name = name;
	}
	
	public Client(String name) {
		super();
		this.name = name;
	}

	public Client() {
		super();
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Position> getPositions() {
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}	
	
}
