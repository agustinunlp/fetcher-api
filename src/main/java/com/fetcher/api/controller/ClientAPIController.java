package com.fetcher.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fetcher.api.dto.PositionDTO;
import com.fetcher.api.exception.PositionServiceException;
import com.fetcher.api.service.IPositionService;

import io.swagger.annotations.ApiOperation;
 
@RestController
@RequestMapping("/api")
public class ClientAPIController {
 
    public static final Logger logger = LoggerFactory.getLogger(ClientAPIController.class);
    
    @Autowired
    IPositionService positionService; 
    
	/**
	 * Retrieve all positions filtered by location, name and type
	 * @param client
	 * @param location
	 * @param name
	 * @param type
	 * @return
	 */
    @ApiOperation(value = "Retrieve all positions filtered by location, name and type")
    @RequestMapping(path = "/positions/{client}", method = RequestMethod.GET)
    public ResponseEntity<List<PositionDTO>> positionsByLocation(
    		@RequestParam(required = false) String location,
    		@RequestParam(required = false) String name,
    		@RequestParam(required = false) String type,
    		@PathVariable("client") String client) {
    	List<PositionDTO> positions = null;
		try {
			positions = positionService.getPositions(location, name, type, client);
		} catch (PositionServiceException e) {
			 return new ResponseEntity<List<PositionDTO>>(positions, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        return new ResponseEntity<List<PositionDTO>>(positions, HttpStatus.OK);
    } 
     
}
