package com.fetcher.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fetcher.api.exception.PositionRepositoryException;
import com.fetcher.api.request.ImportLocationRequest;
import com.fetcher.api.service.IImportPositionService;

import io.swagger.annotations.ApiOperation;
 
@RestController
@RequestMapping("/api")
public class ImportPositionController {
 
    public static final Logger logger = LoggerFactory.getLogger(ImportPositionController.class);
    
    @Autowired
    IImportPositionService importService; 
    
    /**
     * Import positions from external API
     * @param RequestBody
     * @return
     */
    
    @ApiOperation(value = "Import positions from external API")
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public ResponseEntity<String> locations(@Valid @RequestBody ImportLocationRequest request) {
    	try {
			importService.importPositions(request.getUrl(), request.getCount());
		} catch (PositionRepositoryException e) {
			  return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
        return new ResponseEntity<String>(HttpStatus.OK);
    } 
     
}
