package com.fetcher.api.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ImportLocationRequest {
	
	@NotBlank(message = "Url is mandatory")
	private String url;

	@Min(1)
	private int count;	
	
	public ImportLocationRequest() {
		super();
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}

}
