package com.fetcher.api.dao;

import com.fetcher.api.domain.PositionType;
import com.fetcher.api.exception.DAOException;

public interface IPositionTypeDAO {

	PositionType getPositionTypeByTypeName(String type) throws DAOException;

}
