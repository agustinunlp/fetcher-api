package com.fetcher.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fetcher.api.dao.IPositionTypeDAO;
import com.fetcher.api.domain.PositionType;
import com.fetcher.api.exception.DAOException;

@Repository

public class PositionTypeDAOImpl implements IPositionTypeDAO{
	private static final String TYPE = "type";

	@PersistenceContext
	private EntityManager em;
	
	public static final Logger logger = LoggerFactory.getLogger(PositionTypeDAOImpl.class);

	@Override
	public PositionType getPositionTypeByTypeName(String type) throws DAOException{
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<PositionType> query = builder.createQuery(PositionType.class);
			Root<PositionType> root = query.from(PositionType.class);
			query.select(root).where(builder.equal(root.get(TYPE), type));
			TypedQuery<PositionType> tq = em.createQuery(query);
			List<PositionType> positionType = tq.getResultList();
			if(!positionType.isEmpty()){
				return positionType.get(0);			
			}
			return null;
		} catch (Exception e) {
			logger.error("Cannot retrieve position type by type name: {}",type);
			throw new DAOException(e);
		}
	}
}
