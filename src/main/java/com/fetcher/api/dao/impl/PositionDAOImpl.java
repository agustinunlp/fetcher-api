package com.fetcher.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fetcher.api.dao.IClientDAO;
import com.fetcher.api.dao.IPositionDAO;
import com.fetcher.api.dao.IPositionTypeDAO;
import com.fetcher.api.domain.Client;
import com.fetcher.api.domain.Position;
import com.fetcher.api.domain.PositionType;
import com.fetcher.api.exception.DAOException;

/**
 * Position DAO 
 * @author agustin
 *
 */
@Repository
public class PositionDAOImpl implements IPositionDAO{
	private static final String EXTERNAL_ID = "externalId";

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private IPositionTypeDAO positionTypeDAO;

	@Autowired
	private IClientDAO clientDAO;

	public static final Logger logger = LoggerFactory.getLogger(PositionDAOImpl.class);

	@Override
	public Position getPositionByExternalId(String id) throws DAOException{
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<Position> query = builder.createQuery(Position.class);
			Root<Position> root = query.from(Position.class);
			query.select(root).where(builder.equal(root.get(EXTERNAL_ID), id));
			TypedQuery<Position> tq = em.createQuery(query);
			List<Position> positions = tq.getResultList();
			if(!positions.isEmpty()){
				return positions.get(0);									
			}
			return null;
		} catch (Exception e) {
			logger.error("Cannot retrieve position by externalID: {}",id);
			throw new DAOException("",e);
		}
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public void upsertPosition(Position position) throws DAOException {	
		try {
			PositionType positionType = positionTypeDAO.getPositionTypeByTypeName(position.getType().getType());
			if(positionType != null){
				position.setType(positionType);
			}

			Client client = clientDAO.getClientByName(position.getCurrentCompany().getName());
			if(client != null){
				position.setCurrentCompany(client);
			}

			Position existingPosition = getPositionByExternalId(position.getExternalId());
			if(existingPosition == null){
				//Save
				em.persist(position);					
			}
			else{
				//Update
				existingPosition.setValueFrom(position);
				em.merge(existingPosition);
			}
		} catch (PersistenceException e) {
			logger.error("Cannot save/update position with externalId: {}",position.getExternalId());
			throw new DAOException(e);
		}

	}
}
