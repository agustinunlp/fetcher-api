package com.fetcher.api.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.fetcher.api.dao.IClientDAO;
import com.fetcher.api.domain.Client;
import com.fetcher.api.exception.DAOException;

/**
 * Client DAO 
 * @author agustin
 *
 */
@Repository
public class ClientDAOImpl implements IClientDAO{
	private static final String NAME = "name";

	@PersistenceContext
	private EntityManager em;
	
	public static final Logger logger = LoggerFactory.getLogger(ClientDAOImpl.class);

	@Override
	public Client getClientByName(String name) throws DAOException {
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<Client> query = builder.createQuery(Client.class);
			Root<Client> root = query.from(Client.class);
			query.select(root).where(builder.equal(root.get(NAME), name));
			TypedQuery<Client> tq = em.createQuery(query);
			List<Client> clientList = tq.getResultList();
			if(!clientList.isEmpty()){
				return clientList.get(0);							
			}
			return null;
		} catch (Exception e) {
			logger.error("Cannot retrieve client information from the database for: {}",name);
			throw new DAOException(e);
		}

	}
}
