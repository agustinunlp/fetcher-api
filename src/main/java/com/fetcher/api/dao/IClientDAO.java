package com.fetcher.api.dao;

import com.fetcher.api.domain.Client;
import com.fetcher.api.exception.DAOException;

public interface IClientDAO {

	Client getClientByName(String name) throws DAOException;
	
}
