package com.fetcher.api.dao;

import com.fetcher.api.domain.Position;
import com.fetcher.api.exception.DAOException;

public interface IPositionDAO {

	Position getPositionByExternalId(String id) throws DAOException;
  
	void upsertPosition(Position position) throws DAOException;

}
