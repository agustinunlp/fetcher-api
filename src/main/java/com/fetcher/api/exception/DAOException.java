package com.fetcher.api.exception;

public class DAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7667636153741497384L;
	
	public DAOException(){
		super();
	}
	
	public DAOException(String message, Throwable cause) {
		super(message, cause); 
	}
	
	public DAOException(Throwable cause) {
		super(cause); 
	}


}
