package com.fetcher.api.exception;

public class PositionServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2835165067763045963L;

	public PositionServiceException() {
		super();
	}

	public PositionServiceException(String message, Throwable cause) {
		super(message, cause); 
	}
	
	public PositionServiceException(Throwable cause) {
		super(cause); 
	}

}
