package com.fetcher.api.exception;

public class PositionRepositoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4183237575309801981L;
	
	public PositionRepositoryException() {
		super();
	}

	public PositionRepositoryException(String message, Throwable cause) {
		super(message, cause); 
	}
	
	public PositionRepositoryException(Throwable cause) {
		super(cause); 
	}


}
