package com.fetcher.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fetcher.api.domain.Client;
import com.fetcher.api.domain.Position;
import com.fetcher.api.domain.PositionType;

public class ImportPositionDTO{
	private String id;
	private String type;
	private String url;
	@JsonProperty("created_at")
	private String createdAt;
	private String company;
	@JsonProperty("company_url")
	private String companyUrl;
	private String location;
	private String title;
	private String description;
	@JsonProperty("how_to_apply")
	private String howToApply;
	@JsonProperty("company_logo")
	private String companyLogo;	
	
	public Position convert() {
		return new Position(this.getId(), this.getTitle(), new Client(this.getCompany()), new PositionType(this.type), this.getLocation());
	}
	public ImportPositionDTO(){
		super();
	}
	public ImportPositionDTO(String id, String type, String company, String location, String title) {
		super();
		this.id = id;
		this.type = type;
		this.company = company;
		this.location = location;
		this.title = title;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCompanyUrl() {
		return companyUrl;
	}
	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getHowToApply() {
		return howToApply;
	}
	public void setHowToApply(String howToApply) {
		this.howToApply = howToApply;
	}
	public String getCompanyLogo() {
		return companyLogo;
	}
	public void setCompanyLogo(String companyLogo) {
		this.companyLogo = companyLogo;
	}
}
