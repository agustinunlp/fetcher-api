package com.fetcher.api.dto;

public class PositionDTO {
	private String externalId;
	private String name;
	private String currentCompany;
	private String type;
	private String location;	
	
	public PositionDTO(String externalId, String name, String currentCompany, String type, String location) {
		super();
		this.externalId = externalId;
		this.name = name;
		this.currentCompany = currentCompany;
		this.type = type;
		this.location = location;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCurrentCompany() {
		return currentCompany;
	}
	public void setCurrentCompany(String currentCompany) {
		this.currentCompany = currentCompany;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

}
