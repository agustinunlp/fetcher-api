package com.fetcher.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fetcher.api.dao.IPositionDAO;
import com.fetcher.api.dto.ImportPositionDTO;
import com.fetcher.api.exception.DAOException;
import com.fetcher.api.exception.PositionRepositoryException;
import com.fetcher.api.repository.IPositionRepository;
import com.fetcher.api.service.IImportPositionService;

/**
 * Import Service get positions from an API and save them into the DB 
 * @author agustin
 *
 */
@Service
public class ImportPositionServiceImpl implements IImportPositionService {
	@Autowired
	private IPositionRepository positionsRepository;

	@Autowired 
	private IPositionDAO positionDAO;

	public static final Logger logger = LoggerFactory.getLogger(ImportPositionServiceImpl.class);

	@Override
	public void importPositions(String url, int count) throws PositionRepositoryException {
		logger.info("Start Importing {} positions from {}", count, url);
		
		int pages = this.getPages(count);

		List<ImportPositionDTO> positionsDTOList = getPositionsFromRepository(url, pages);

		List<ImportPositionDTO> filteredDTOCollection = positionsDTOList.stream().limit(count).collect(Collectors.toList());	

		filteredDTOCollection.stream().forEach(dto->{
			try {
				positionDAO.upsertPosition(dto.convert());
			} catch (DAOException e) {
				logger.error(e.getMessage());
			}
		});

		logger.info("End Importing {} positions from {}", count, url);

	}
	
	private List<ImportPositionDTO> getPositionsFromRepository(String url, int pages) throws PositionRepositoryException {
		List<ImportPositionDTO> positionsDTOList = new ArrayList<>();
		for (int i = 1; i <= pages; i++) {
			positionsDTOList.addAll(positionsRepository.getPositions(url, i));
		}
		return positionsDTOList;
	}

	public int getPages(int count) {
		int pages = count/50;
		if(count % 50 == 1 || pages == 0){
			pages++;
		}

		return pages;
	}

}
