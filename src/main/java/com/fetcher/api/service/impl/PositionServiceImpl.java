package com.fetcher.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fetcher.api.dao.IClientDAO;
import com.fetcher.api.domain.Client;
import com.fetcher.api.domain.Position;
import com.fetcher.api.dto.PositionDTO;
import com.fetcher.api.exception.PositionServiceException;
import com.fetcher.api.service.IPositionService;
/**
 * PositionService provide functionality to get and filter positions
 * @author agustin
 *
 */
@Service
public class PositionServiceImpl implements IPositionService {

	@Autowired 
	private IClientDAO clientDAO;
	
	public static final Logger logger = LoggerFactory.getLogger(PositionServiceImpl.class);

	@Override
	public List<PositionDTO> getPositions(String location, String name, String type, String clientName) throws PositionServiceException {
		logger.info("Get positions from {}", clientName);

		List<PositionDTO> positionDTOList = new ArrayList<>();

		try {
			Client client = clientDAO.getClientByName(clientName);
			List<Position> positions = client.getPositions();		

			if(!StringUtils.isEmpty(name)){
				positions =  positions.stream().filter(pos-> pos.getName().contains(name)).collect(Collectors.toList());
			}
			if(!StringUtils.isEmpty(type)){
				positions =  positions.stream().filter(pos-> pos.getType().getType().equals(type)).collect(Collectors.toList());
			}
			if(!StringUtils.isEmpty(location)){
				positions =  positions.stream().filter(pos-> pos.getLocation().equals(location)).collect(Collectors.toList());
			}

			for (Position position : positions) {
				positionDTOList.add(position.convertToDTO());
			}
		} catch (Exception e) {
			logger.error("Error retrieving positions for: {}",clientName);
			throw new PositionServiceException("Error retrieving positions for: "+clientName,e);
		}
		return positionDTOList;
	}

}
