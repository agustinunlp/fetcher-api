package com.fetcher.api.service;

import com.fetcher.api.exception.PositionRepositoryException;

public interface IImportPositionService {
	
	void importPositions(String url, int count) throws PositionRepositoryException;
}
