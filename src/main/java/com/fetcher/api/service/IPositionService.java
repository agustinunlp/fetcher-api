package com.fetcher.api.service;

import java.util.List;

import com.fetcher.api.dto.PositionDTO;
import com.fetcher.api.exception.PositionServiceException;

public interface IPositionService {

	List<PositionDTO> getPositions(String location, String name, String type, String client) throws PositionServiceException;
}
 